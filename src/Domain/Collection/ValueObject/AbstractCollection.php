<?php
declare(strict_types=1);

namespace App\Domain\Collection\ValueObject;

use Iterator;
use App\Domain\Collection\CollectionElementInterface;
use App\Domain\Collection\CollectionException;

abstract class AbstractCollection implements Iterator
{
    /** @var int  */
    private $iterator = 0;

    /** @var CollectionElementInterface[] */
    protected $collection = [];

    /** @var string */
    protected $collectionType;

    public function __construct()
    {
        $this->collectionType = 'classNameOfThisCollection';
    }

    /**
     * @param CollectionElementInterface $element
     * @throws CollectionException
     */
    public function addElement($element)
    {
        if (!($element instanceof $this->collectionType)) {
            throw CollectionException::buildInvalidElementType(get_class($element), $this->collectionType);
        }

        $this->collection[] = $element;
    }

    /**
     * @param int $id
     * @return CollectionElementInterface
     */
    public function getElement(int $id)
    {
        return $this->collection[$id];
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return count($this->collection);
    }

     /**
      * Return the current element
      * @link http://php.net/manual/en/iterator.current.php
      * @return mixed Can return any type.
      * @since 5.0.0
      */
     public function current()
     {
         return $this->collection[$this->iterator];
     }

     /**
      * Move forward to next element
      * @link http://php.net/manual/en/iterator.next.php
      * @return void Any returned value is ignored.
      * @since 5.0.0
      */
     public function next()
     {
         $this->iterator++;
     }

     /**
      * Return the key of the current element
      * @link http://php.net/manual/en/iterator.key.php
      * @return mixed scalar on success, or null on failure.
      * @since 5.0.0
      */
     public function key()
     {
         return $this->iterator;
     }

     /**
      * Checks if current position is valid
      * @link http://php.net/manual/en/iterator.valid.php
      * @return boolean The return value will be casted to boolean and then evaluated.
      * Returns true on success or false on failure.
      * @since 5.0.0
      */
     public function valid()
     {
         return isset($this->collection[$this->iterator]);
     }

     /**
      * Rewind the Iterator to the first element
      * @link http://php.net/manual/en/iterator.rewind.php
      * @return void Any returned value is ignored.
      * @since 5.0.0
      */
     public function rewind()
     {
         $this->iterator = 0;
     }
 }
