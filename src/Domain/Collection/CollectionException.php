<?php
declare(strict_types=1);

namespace App\Domain\Collection;

class CollectionException extends \Exception
{
    /**
     * @param string $wrong
     * @param string $proper
     * @return CollectionException
     */
    public static function buildInvalidElementType(string $wrong, string $proper) {
        return new CollectionException("Trying to add element of type " . $wrong . " expected: " . $proper);
    }
}