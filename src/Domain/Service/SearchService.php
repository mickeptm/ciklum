<?php
declare(strict_types = 1);

namespace App\Domain\Service;

use App\Application\DomainInterface\Service\SearchServiceInterface;
use App\Domain\Repository\GithubRepository;
use App\Domain\ValueObject\SearchResultCollection;

class SearchService implements SearchServiceInterface
{
    /** @var GithubRepository */
    private $repository;

    /**
     * SearchService constructor.
     * @param GithubRepository $repository
     */
    public function __construct(GithubRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $phrase
     * @param string $repository
     * @param null|string $context
     * @param null|string $language
     * @param $page
     * @param $pageSize
     * @return SearchResultCollection
     * @throws \App\Domain\Collection\CollectionException
     * @throws \App\Domain\Exception\ApiException
     */
    public function search(
        string $phrase,
        string $repository,
        ?string $context,
        ?string $language,
        int $page,
        int $pageSize,
        string $sort,
        string $order
    ): SearchResultCollection {
        $searchPhrase = $phrase . "+repo:" . $repository;

        if (!is_null($context)) {
            $searchPhrase .=  '+in:' . $context;
        }

        if (!is_null($language)) {
            $searchPhrase .=  '+language:' . $language;
        }

        return $this->repository->searchForRecords($searchPhrase, $page, $pageSize, $sort, $order);
    }
}