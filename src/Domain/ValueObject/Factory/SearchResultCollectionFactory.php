<?php
declare(strict_types = 1);

namespace App\Domain\ValueObject\Factory;

use App\Domain\ValueObject\SearchResult;
use App\Domain\ValueObject\SearchResultCollection;

class SearchResultCollectionFactory
{
    /**
     * @param \stdClass $data
     * @return SearchResultCollection
     * @throws \App\Domain\Collection\CollectionException
     */
    public function buildFromApiResponse(\stdClass $data): SearchResultCollection
    {
        $searchResultCollection = new SearchResultCollection();
        foreach ($data->items as $item) {
            $searchResultCollection->addElement(new SearchResult(
                $item->repository->owner->login,
                $item->repository->full_name,
                $item->name,
                $item->html_url
            ));
        }

        $searchResultCollection->setTotalCount($data->total_count);

        return $searchResultCollection;
    }
}