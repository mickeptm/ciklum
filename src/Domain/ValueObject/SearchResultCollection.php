<?php
declare(strict_types = 1);

namespace App\Domain\ValueObject;

use App\Domain\Collection\ValueObject\AbstractCollection;

class SearchResultCollection extends AbstractCollection implements \JsonSerializable
{
    /** @var int */
    private $totalCount = 0;

    /**
     * SearchResultCollection constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->collectionType = SearchResult::class;
    }

    /**
     * @param SearchResult $element
     * @throws \App\Domain\Collection\CollectionException
     */
    public function addElement($element)
    {
        parent::addElement($element);
    }

    /**
     * @param int $id
     * @return SearchResult
     */
    public function getElement(int $id)
    {
        return parent::getElement($id);
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     */
    public function setTotalCount(int $totalCount): void
    {
        $this->totalCount = $totalCount;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->collection;
    }
}