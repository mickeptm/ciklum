<?php
declare(strict_types = 1);

namespace App\Domain\ValueObject;

use App\Domain\Collection\CollectionElementInterface;

class SearchResult implements CollectionElementInterface, \JsonSerializable
{
    /** @var string */
    private $ownerName;
    /** @var string */
    private $repositoryName;
    /** @var string */
    private $fileName;
    /** @var string */
    private $repositoryUrl;

    /**
     * SearchResult constructor.
     * @param string $ownerName
     * @param string $repositoryName
     * @param string $fileName
     * @param string $repositoryUrl
     */
    public function __construct(string $ownerName, string $repositoryName, string $fileName, string $repositoryUrl)
    {
        $this->ownerName = $ownerName;
        $this->repositoryName = $repositoryName;
        $this->fileName = $fileName;
        $this->repositoryUrl = $repositoryUrl;
    }

    /**
     * @return string
     */
    public function getOwnerName(): string
    {
        return $this->ownerName;
    }

    /**
     * @return string
     */
    public function getRepositoryName(): string
    {
        return $this->repositoryName;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getRepositoryUrl(): string
    {
        return $this->repositoryUrl;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'ownerName' => $this->getOwnerName(),
            'repositoryName' => $this->getRepositoryName(),
            'fileName' => $this->getFileName(),
            'repositoryUrl' => $this->getRepositoryUrl()
        ];
    }
}