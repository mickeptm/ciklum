<?php
declare(strict_types = 1);

namespace App\Domain\Repository;

use App\Domain\Exception\ApiException;
use App\Domain\ValueObject\Factory\SearchResultCollectionFactory;
use App\Domain\ValueObject\SearchResultCollection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class GithubRepository
{
    const GITHUB_CODE_SEARCH_URL = 'https://api.github.com/search/code';

    /** @var Client */
    protected $httpClient;

    /**
     * GithubRepository constructor.
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $searchPhrase
     * @param int $page
     * @param int $resultCount
     * @param string $sort
     * @param string $order
     * @return SearchResultCollection
     * @throws ApiException
     * @throws \App\Domain\Collection\CollectionException
     */
    public function searchForRecords(string $searchPhrase, $page = 0, int $resultCount = 25, string $sort = 'score', string $order = 'desc'): SearchResultCollection
    {
        try {
            $response = $this->httpClient->get(
                self::GITHUB_CODE_SEARCH_URL .
                '?q=' . $searchPhrase .
                '&page=' . $page .
                '&per_page=' . $resultCount .
                '&sort=' . $sort .
                '&order=' . $order
            );

            if ($response->getStatusCode() !== 200) {
                throw new ApiException("Response code was " . $response->getStatusCode() . ', not 200', $response->getStatusCode());
            }

        } catch (ClientException $exception) {
            throw new ApiException($exception->getMessage(), $exception->getCode());
        }

        $factory = new SearchResultCollectionFactory();
        return $factory->buildFromApiResponse(\GuzzleHttp\json_decode($response->getBody()->getContents()));
    }
}