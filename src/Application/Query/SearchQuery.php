<?php

namespace App\Application\Query;

class SearchQuery
{
    const PROPER_CONTEXT_VALUES = ['file', 'path', null];
    const PROPER_SORT_VALUES = ['score', 'indexed'];
    const PROPER_ORDER_VALUES = ['asc', 'desc'];

    /** @var string */
    private $phrase;
    /** @var string */
    private $repository;
    /** @var null|string */
    private $context = null;
    /** @var null|string */
    private $language = null;
    /** @var int */
    private $page;
    /** @var int */
    private $pageSize;
    /** @var string */
    private $sort;
    /** @var string */
    private $order;

    /**
     * SearchQuery constructor.
     * @param string $phrase
     * @param string $repository
     * @param null|string $context
     * @param null|string $language
     * @param int $page
     * @param int $pageSize
     */
    public function __construct(
        string $phrase,
        string $repository,
        ?string $context,
        ?string $language,
        int $page,
        int $pageSize,
        string $sort,
        string $order
    ) {
        $this->phrase = $phrase;
        $this->repository = $repository;
        $this->context = $context;
        $this->language = $language;
        $this->page = $page;
        $this->pageSize = $pageSize;
        $this->sort = $sort;
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getPhrase(): string
    {
        return $this->phrase;
    }

    /**
     * @return string
     */
    public function getRepository(): string
    {
        return $this->repository;
    }

    /**
     * @return null|string
     */
    public function getContext(): ?string
    {
        return $this->context;
    }

    /**
     * @return null|string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return string
     */
    public function getSort(): string
    {
        return $this->sort;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

}