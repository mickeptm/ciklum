<?php
declare(strict_types = 1);

namespace App\Application\Query\Factory;

use App\Application\Exception\ValidationException;
use App\Application\Query\SearchQuery;
use Symfony\Component\HttpFoundation\Request;

class SearchQueryFactory
{
    /**
     * @param Request $request
     * @return SearchQuery
     * @throws ValidationException
     */
    public static function buildFromSymfonyRequest(Request $request)
    {
        $query = $request->query;

        if (!($query->has('phrase')) || strlen($query->get('phrase')) <= 3) {
            throw new ValidationException('Parameter: "phrase" must be present and longer than 3 characters', 400);
        }

        if (!($query->has('repository'))) {
            throw new ValidationException('Parameter: "repository" must be present', 400);
        }

        if ($query->has('context') && ! in_array($query->get('context'), SearchQuery::PROPER_CONTEXT_VALUES)) {
            throw new ValidationException('Parameter: "context" must have one of following values: "path", "file"', 400);
        }

        if ($query->has('sort') && ! in_array($query->get('sort'), SearchQuery::PROPER_SORT_VALUES)) {
            throw new ValidationException('Parameter: "sort" must have one of following values: "score", "indexed"', 400);
        }

        if ($query->has('order') && ! in_array($query->get('order'), SearchQuery::PROPER_ORDER_VALUES)) {
            throw new ValidationException('Parameter: "sort" must have one of following values: "asc", "desc"', 400);
        }

        return new SearchQuery(
            $query->get('phrase'),
            $query->get('repository'),
            $query->get('context', null),
            $query->get('language', null),
            $query->getInt('page', 0),
            $query->getInt('perPage', 25),
            $query->get('sort', 'score'),
            $query->get('order', 'desc')
        );
    }
}