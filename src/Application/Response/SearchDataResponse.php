<?php
declare(strict_types = 1);

namespace App\Application\Response;

use App\Domain\ValueObject\SearchResultCollection;

class SearchDataResponse extends AbstractApiResponse
{
    const HTTP_OK = 200;

    /** @var SearchResultCollection */
    private $dataCollection;

    /**
     * SearchDataResponse constructor.
     * @param SearchResultCollection $dataCollection
     */
    public function __construct(SearchResultCollection $dataCollection)
    {
        $this->dataCollection = $dataCollection;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'total' => $this->dataCollection->getTotalCount(),
            'current' => $this->dataCollection->getLength(),
            'data' => $this->dataCollection
        ];
    }

    /**
     * @return int
     */
    public function getHttpStatus(): int
    {
        return self::HTTP_OK;
    }

    /**
     * @return string
     */
    public function getResponseBody(): string
    {
        return json_encode($this);
    }
}