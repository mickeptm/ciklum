<?php
declare(strict_types = 1);

namespace App\Application\Response;

abstract class AbstractApiResponse implements \JsonSerializable
{
    public abstract function getHttpStatus(): int;

    public abstract function getResponseBody(): string;
}