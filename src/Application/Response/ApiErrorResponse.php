<?php
declare(strict_types = 1);

namespace App\Application\Response;

class ApiErrorResponse extends AbstractApiResponse
{
    const HTTP_BAD_REQUEST = 400;
    const HTTP_INTERNAL_ERROR = 500;

    /** @var string */
    private $message;

    /** @var int */
    private $status;

    /**
     * SearchDataError constructor.
     * @param string $message
     * @param int $status
     */
    public function __construct(string $message, int $status)
    {
        $this->message = $message;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'message' => $this->getMessage(),
            'status' => $this->getHttpStatus()
        ];
    }

    /**
     * @return int
     */
    public function getHttpStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getResponseBody(): string
    {
        return json_encode($this);
    }
}