<?php

namespace App\Application\Service;

use App\Application\DomainInterface\Service\SearchServiceInterface;
use App\Application\Query\SearchQuery;
use App\Application\Response\AbstractApiResponse;
use App\Application\Response\ApiErrorResponse;
use App\Application\Response\SearchDataResponse;
use App\Domain\Exception\ApiException;
use Psr\Log\LoggerInterface;

class SearchService
{
    /** @var SearchServiceInterface */
    private $domainSearchService;

    /** @var LoggerInterface */
    private $logger;

    /**
     * SearchService constructor.
     * @param SearchServiceInterface $domainSearchService
     * @param LoggerInterface $logger
     */
    public function __construct(SearchServiceInterface $domainSearchService, LoggerInterface $logger)
    {
        $this->domainSearchService = $domainSearchService;
        $this->logger = $logger;
    }

    /**
     * @param SearchQuery $query
     * @return AbstractApiResponse
     */
    public function searchData(SearchQuery $query): AbstractApiResponse
    {
        try {
            return new SearchDataResponse($this->domainSearchService->search(
                $query->getPhrase(),
                $query->getRepository(),
                $query->getContext(),
                $query->getLanguage(),
                $query->getPage(),
                $query->getPageSize(),
                $query->getSort(),
                $query->getOrder()
            ));
        } catch (ApiException $e) {
            return new ApiErrorResponse($e->getMessage(), ApiErrorResponse::HTTP_BAD_REQUEST);
        } catch (\Throwable $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            return new ApiErrorResponse('There is some internal problem, please try again later', ApiErrorResponse::HTTP_INTERNAL_ERROR);
        }
    }
}