<?php
declare(strict_types = 1);
namespace App\Application\DomainInterface\Service;

use App\Domain\Exception\ApiException;
use App\Domain\ValueObject\SearchResultCollection;

interface SearchServiceInterface
{
    /**
     * @param string $phrase
     * @param string $repository
     * @param null|string $context
     * @param null|string $language
     * @param int $page
     * @param int $pageSize
     * @param string $sort
     * @param string $order
     * @return SearchResultCollection
     * @throws ApiException
     */
    public function search(
        string $phrase,
        string $repository,
        ?string $context,
        ?string $language,
        int $page,
        int $pageSize,
        string $sort,
        string $order
    ): SearchResultCollection;
}