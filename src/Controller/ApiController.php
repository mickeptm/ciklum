<?php

namespace App\Controller;

use App\Application\Exception\ValidationException;
use App\Application\Query\Factory\SearchQueryFactory;
use App\Application\Response\ApiErrorResponse;
use App\Application\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{
    /**
     * @param Request $request
     * @param SearchService $appService
     * @return JsonResponse
     *
     * @Route("/code/search", name="code.search", methods={"GET"})
     */
    public function searchCode(
        Request $request,
        SearchService $appService
    ) {
        try {
            $query = SearchQueryFactory::buildFromSymfonyRequest($request);
            $applicationResponse = $appService->searchData($query);
        } catch (ValidationException $e) {
            $applicationResponse = new ApiErrorResponse($e->getMessage(), $e->getCode());
        }

        return new JsonResponse(
            $applicationResponse->getResponseBody(),
            $applicationResponse->getHttpStatus(),
            [],
            true
        );
    }
}