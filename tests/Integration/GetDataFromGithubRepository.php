<?php
namespace App\Tests\Integration;

use App\Domain\Repository\GithubRepository;
use App\Domain\ValueObject\SearchResultCollection;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class GetDataFromGithubRepository extends TestCase
{
    /**
     * @test
     * @throws \App\Domain\Collection\CollectionException
     * @throws \App\Domain\Exception\ApiException
     */
    public function shouldReturn9RecordsFromGitHubAboutAddClass()
    {
        $repo = new GithubRepository(new Client());
        $resp = $repo->searchForRecords('addClass+repo:jQuery/jQuery', 0, 25);
        self::assertInstanceOf(SearchResultCollection::class, $resp);
        self::assertEquals(9, $resp->getLength());
        self::assertEquals(9, $resp->getTotalCount());
    }
}
