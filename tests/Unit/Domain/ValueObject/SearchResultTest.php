<?php

namespace App\Tests\Unit\Domain\ValueObject;

use App\Domain\ValueObject\SearchResult;
use PHPUnit\Framework\TestCase;

class SearchResultTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnJson()
    {
        $searchResult = new SearchResult(
            'aaa',
            'bbb',
            'ccc',
            'ddd'
        );

        $json = json_encode($searchResult);
        self::assertJsonStringEqualsJsonString(
            json_encode([
                'ownerName' => 'aaa',
                'repositoryName' => 'bbb',
                'fileName' => 'ccc',
                'repositoryUrl' => 'ddd'
            ]),
            $json
        );
    }
}
