<?php

namespace App\Tests\Unit\Domain\ValueObject\Factory;

use App\Domain\ValueObject\Factory\SearchResultCollectionFactory;
use App\Domain\ValueObject\SearchResult;
use App\Domain\ValueObject\SearchResultCollection;
use PHPUnit\Framework\TestCase;

class SearchResultCollectionFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnSearchResultCollection()
    {
        $factory = new SearchResultCollectionFactory();

        $result = $factory->buildFromApiResponse($this->getDataObject());

        self::assertInstanceOf(SearchResultCollection::class, $result);
        self::assertEquals(1, $result->getLength());
        self::assertEquals(10, $result->getTotalCount());
        self::assertInstanceOf(SearchResult::class, $result->getElement(0));
    }

    public function getDataObject()
    {
        return (object) [
            'total_count' => 10,
            'items' =>[
                (object) [
                    'name' => 'aaa',
                    'html_url' => 'http://some.url.com',
                    'repository' => (object) [
                        'full_name' => 'repoName',
                        'owner' => (object) [
                            'login' => 'user'
                        ]
                    ]
                ]
            ]
        ];
    }
}
