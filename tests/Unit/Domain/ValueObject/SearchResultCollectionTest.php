<?php

namespace App\Tests\Unit\Domain\ValueObject;

use App\Domain\ValueObject\SearchResult;
use App\Domain\ValueObject\SearchResultCollection;
use PHPUnit\Framework\TestCase;

class SearchResultCollectionTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnJson()
    {
        $searchResult = new SearchResult(
            'aaa',
            'bbb',
            'ccc',
            'ddd'
        );

        $searchResult2 = new SearchResult(
            'aaa1',
            'bbb1',
            'ccc1',
            'ddd1'
        );

        $collection = new SearchResultCollection();

        $collection->addElement($searchResult);
        $collection->addElement($searchResult2);

        $json = json_encode($collection);
        self::assertJsonStringEqualsJsonString(
            json_encode([
                    [
                        'ownerName' => 'aaa',
                        'repositoryName' => 'bbb',
                        'fileName' => 'ccc',
                        'repositoryUrl' => 'ddd'
                    ],
                    [
                        'ownerName' => 'aaa1',
                        'repositoryName' => 'bbb1',
                        'fileName' => 'ccc1',
                        'repositoryUrl' => 'ddd1'
                    ]
                ]),
            $json
        );
    }
}
