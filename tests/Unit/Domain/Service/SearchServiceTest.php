<?php
declare(strict_types = 1);
namespace App\Tests\Unit\Domain\Service;

use App\Application\DomainInterface\Service\SearchServiceInterface;
use App\Domain\Repository\GithubRepository;
use App\Domain\Service\SearchService;
use App\Domain\ValueObject\SearchResultCollection;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class SearchServiceTest extends TestCase
{
    public function shouldCreateSearchService()
    {
        $service = new SearchService(new GithubRepository(new Client()));

        self::assertInstanceOf(SearchService::class, $service);
        self::assertInstanceOf(SearchServiceInterface::class, $service);
    }

    /**
     * @test
     */
    public function shouldReturnRepositoryResponse()
    {
        $repoMock = $this->prophesize(GithubRepository::class);
        $repoMock->searchForRecords('test+repo:testTest+in:path+language:java', 0, 10, 'score', 'desc')->willReturn(new SearchResultCollection());

        $service = new SearchService($repoMock->reveal());
        $response = $service->search(
            'test',
            'testTest',
            'path',
            'java',
            0,
            10,
            'score',
            'desc'
        );

        self::assertInstanceOf(SearchResultCollection::class, $response);
    }
}
