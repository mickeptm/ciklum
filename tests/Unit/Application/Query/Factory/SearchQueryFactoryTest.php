<?php

namespace App\Tests\Unit\Application\Query\Factory;

use App\Application\Query\Factory\SearchQueryFactory;
use App\Application\Query\SearchQuery;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class SearchQueryFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function shouldBuildSearchQuery()
    {
        $request = new Request([
            'phrase' => 'test',
            'repository' => 'someRepo'
        ]);

        $query = SearchQueryFactory::buildFromSymfonyRequest($request);

        self::assertInstanceOf(SearchQuery::class, $query);
        self::assertEquals('test', $query->getPhrase());
        self::assertEquals('someRepo', $query->getRepository());
        self::assertEquals(null, $query->getLanguage());
        self::assertEquals(null, $query->getContext());
        self::assertEquals(0, $query->getPage());
        self::assertEquals(25, $query->getPageSize());
    }

    /**
     * @test
     * @expectedException \App\Application\Exception\ValidationException
     * @dataProvider requestDataProvider
     */
    public function shouldThrowValidationException($requestData)
    {
        $request = new Request($requestData);

        SearchQueryFactory::buildFromSymfonyRequest($request);
    }

    public function requestDataProvider()
    {
        return [
            [
                [
                    'phrase' => 'te',
                    'repository' => 'someRepo'
                ]
            ],
            [
                [
                    'repository' => 'someRepo'
                ]
            ],
            [
                [
                    'phrase' => 'test',
                ]
            ],
            [
                [
                    'phrase' => 'test',
                    'repository' => 'someRepo',
                    'context' => 'wrong'
                ]
            ]
        ];
    }
}
