<?php

namespace App\Tests\Unit\Application\Response;

use App\Application\Response\ApiErrorResponse;
use PHPUnit\Framework\TestCase;

class ApiErrorResponseTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnJsonString()
    {
        $response = new ApiErrorResponse('Some message', 500);

        self::assertJsonStringEqualsJsonString(json_encode([
            'message' => 'Some message',
            'status' => 500
        ]), $response->getResponseBody());
    }

    /**
     * @test
     */
    public function shouldReturnStatus()
    {
        $response = new ApiErrorResponse('Some message', 500);
        self::assertEquals(500, $response->getHttpStatus());
    }

    /**
     * @test
     */
    public function shouldReturnMessage()
    {
        $response = new ApiErrorResponse('Some message', 500);
        self::assertEquals('Some message', $response->getMessage());
    }
}
