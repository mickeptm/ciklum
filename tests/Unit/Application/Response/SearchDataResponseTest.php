<?php

namespace App\Tests\Unit\Application\Response;

use App\Application\Response\SearchDataResponse;
use App\Domain\ValueObject\SearchResult;
use App\Domain\ValueObject\SearchResultCollection;
use PHPUnit\Framework\TestCase;

class SearchDataResponseTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnJsonString()
    {
        $dataCollection = new SearchResultCollection();
        $dataCollection->addElement(new SearchResult('a', 'b', 'c', 'd'));
        $dataCollection->setTotalCount(5);
        $response = new SearchDataResponse($dataCollection);

        self::assertJsonStringEqualsJsonString(json_encode([
            'total' => 5,
            'current' => 1,
            'data' => [
                [
                    'ownerName' => 'a',
                    'repositoryName' => 'b',
                    'fileName' => 'c',
                    'repositoryUrl' => 'd'
                ]
            ]
        ]), $response->getResponseBody());
    }

    /**
     * @test
     */
    public function shouldReturnStatus()
    {
        $dataCollection = new SearchResultCollection();
        $dataCollection->addElement(new SearchResult('a', 'b', 'c', 'd'));
        $dataCollection->setTotalCount(5);
        $response = new SearchDataResponse($dataCollection);
        self::assertEquals(200, $response->getHttpStatus());
    }
}
