<?php

namespace App\Tests\Unit\Application\Service;

use App\Application\DomainInterface\Service\SearchServiceInterface;
use App\Application\Query\SearchQuery;
use App\Application\Response\ApiErrorResponse;
use App\Application\Response\SearchDataResponse;
use App\Application\Service\SearchService;
use App\Domain\Exception\ApiException;
use App\Domain\ValueObject\SearchResult;
use App\Domain\ValueObject\SearchResultCollection;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class SearchServiceTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnDataResponse()
    {
        $loggerMock = $this->prophesize(LoggerInterface::class);

        $dataCollection = new SearchResultCollection();
        $dataCollection->addElement(new SearchResult('a', 'b', 'c', 'd'));
        $dataCollection->setTotalCount(5);

        $query = new SearchQuery(
            'along text',
            'baba',
            'file',
            'php',
            5,
            25,
            'score',
            'desc'
        );

        $searchServiceMock = $this->prophesize(SearchServiceInterface::class);
        $searchServiceMock->search(
            'along text',
            'baba',
            'file',
            'php',
            5,
            25,
            'score',
            'desc'
        )->willReturn($dataCollection);
        $service = new SearchService($searchServiceMock->reveal(), $loggerMock->reveal());
        $response = $service->searchData($query);

        self::assertInstanceOf(SearchDataResponse::class, $response);
    }

    /**
     * @test
     */
    public function shouldReturnErrorResponse()
    {
        $loggerMock = $this->prophesize(LoggerInterface::class);

        $dataCollection = new SearchResultCollection();
        $dataCollection->addElement(new SearchResult('a', 'b', 'c', 'd'));
        $dataCollection->setTotalCount(5);

        $query = new SearchQuery(
            'along text',
            'baba',
            'file',
            'php',
            5,
            25,
            'score',
            'desc'
        );

        $searchServiceMock = $this->prophesize(SearchServiceInterface::class);
        $searchServiceMock->search(
            'along text',
            'baba',
            'file',
            'php',
            5,
            25,
            'score',
            'desc'
        )->willThrow(new ApiException('Some exception'));
        $service = new SearchService($searchServiceMock->reveal(), $loggerMock->reveal());
        $response = $service->searchData($query);
        self::assertInstanceOf(ApiErrorResponse::class, $response);
    }

    /**
     * @test
     */
    public function shouldReturnErrorResponseFromThrowable()
    {
        $errorException = new \ErrorException('Some exception');
        $loggerMock = $this->prophesize(LoggerInterface::class);
        $loggerMock->critical('Some exception', ['exception' => $errorException])->shouldBeCalled();
        $dataCollection = new SearchResultCollection();
        $dataCollection->addElement(new SearchResult('a', 'b', 'c', 'd'));
        $dataCollection->setTotalCount(5);

        $query = new SearchQuery(
            'along text',
            'baba',
            'file',
            'php',
            5,
            25,
            'score',
            'desc'
        );

        $searchServiceMock = $this->prophesize(SearchServiceInterface::class);
        $searchServiceMock->search(
            'along text',
            'baba',
            'file',
            'php',
            5,
            25,
            'score',
            'desc'
        )->willThrow($errorException);
        $service = new SearchService($searchServiceMock->reveal(), $loggerMock->reveal());
        $response = $service->searchData($query);
        self::assertInstanceOf(ApiErrorResponse::class, $response);
    }
}