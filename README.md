### Project for EASI'r (Ciklum)

As you can see, project was created using symfony4 components for quick and easy development.
Components that were used, were merely the tools to run the code in proper environment. 
Domain and Application code is fully independent and can be moved vrom this environemnt to any other.

Symfony is helping here with is't routing, di, logging, flex and annotations packages, also guzzlehttp was used for connecting to external service.

## Application

Code was written in Domain Driven Design style. Where it is separated for **Infrastructure** (represented by Symfony4 modules), **Business Domain**, **Application** layer and **UI** (symfony Controller).

#### Domain
Inside Domain is placed repository that is used by search service. This repository is essential module that represents communication with github, and it can potentialy (and easliy) be replaced by any other repository file that will support needed data.

Collection folder contains some code that I've moved from my other project. It's representing Domain of Collection, and I'm commonly using it in my projects for representing collection's of objects. However for needs of this project it was simplified.

Other folders contain Domain of search system.

#### Application
Application layer (folder) contains all application relevant classes that uses our Domain (via Interface so it's possible to compleatly replace Domain layer).

#### UI
UserInterface layer is represented by Controller folder that is default for symfony4 project. This layer is compleatly created with using all "of the fun stuff" that symfony gives.

## Tests

There are two type of phpUnit tests in this project. Units, and Integration (that is one integration test with external service of github).
Units are covering all business logic within folders. All tests are placed in tests folder, and have it's own configuration and namespace. They are runed via *symfony/phpunit-bridge*

## What can be better?

It's allways possible to make code better. For example customizing framework modules like error handler for giving more API style responses for situations that are not predicted by given Domain and Application layers.
That was not done here, as It's not target of this exercise. Units possibly could be more strict in data typing. And logging of information could be expanded.
Validation is defiantly a weakness of this project as it's placed directly inside of factory class that is creating a CQRS Query Class and this should not be it's level of expertise according to SOLID rules (which are bent a bit in few places of this code).

## Why DDD?

At this point I see DDD as natural "follow up" for MVC application on code level, and as a good philosophy in code development and communicating with client (or domain experts, or both).